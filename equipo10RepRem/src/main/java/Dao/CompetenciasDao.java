package Dao;

import Conexion.conexion;
import Model.Competencias;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CompetenciasDao extends conexion {

    //Metodo consultar
    public ArrayList<Competencias> consultar() throws Exception {
        ArrayList<Competencias> lista = new ArrayList<>();
        try {
            this.conectar();
            String sql = "select * from competencias";
            PreparedStatement stm = this.getCnx().prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Competencias com = new Competencias();
                com.setId_competencia(rs.getInt("id_competencia"));
                com.setNombre(rs.getString("nombre"));
                com.setDescripcion(rs.getString("descripcion"));
                lista.add(com);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.desconectar();
        }
        return lista;
    }

    public void insertar(Competencias c) throws Exception {
        try {
            this.conectar();
            String sql = "insert into competencias(nombre, descripcion) values(?,?);";
            PreparedStatement stm = this.getCnx().prepareStatement(sql);
            stm.setString(1, c.getNombre());
            stm.setString(2, c.getDescripcion());
            stm.executeUpdate();
        } catch (Exception e) {
        }finally {
            this.desconectar();
        }
    }
}
