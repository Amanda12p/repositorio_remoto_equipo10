/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Conexion.conexion;
import Model.Competencias;
import Model.UsuarioCompetencia;
import Model.Usuarios;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author christian.ramirezusa
 */
public class UsuarioDao extends conexion {

    PreparedStatement ps;
    ResultSet rs;

    String sql;
    
    public List<Usuarios> listaUsuariosAll() throws Exception{
        List<Usuarios> list = new ArrayList();
        sql = "SELECT * FROM Usuarios";
        
        try {
            this.conectar();
            ps = this.getCnx().prepareStatement(sql);
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Usuarios usu = new Usuarios();
                usu.setId_usuario(rs.getInt(1));
                usu.setNombre(rs.getString(2));
                usu.setApellido(rs.getString(3));
                usu.setUsuario(rs.getString(4));
                usu.setPass(rs.getString(5));
                list.add(usu);
            }
            return list;
        } catch (Exception e) {
            throw  e;
        }finally{
            this.desconectar();
        }
    }

    public List<UsuarioCompetencia> listaUsuariosCom() throws Exception {

        List<UsuarioCompetencia> list = new ArrayList<>();
        sql = "SELECT b.nombre,b.apellido, c.nombre FROM usuario_competencia a\n"
                + "INNER JOIN usuarios b ON b.id_usuario = a.id_usuario\n"
                + "INNER JOIN competencias c ON c.id_competencia = a.id_competencia;";

        try {
            this.conectar();
            ps = this.getCnx().prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                UsuarioCompetencia ucom = new UsuarioCompetencia();
                Usuarios u = new Usuarios();
                Competencias com = new Competencias();

                u.setNombre(rs.getString(1));
                u.setApellido(rs.getString(2));
                com.setNombre(rs.getString(3));
                ucom.setUsuarios(u);
                ucom.setCompetencias(com);

                list.add(ucom);
            }
            //return list;
        } catch (Exception e) {
            throw e;
        } finally {
            this.desconectar();
        }
        return list;
    }

    public List<Usuarios> login(Usuarios u) {
        sql = "SELECT * FROM Usuarios WHERE usuario = ? AND pass = ?";
        List<Usuarios> list = new ArrayList<>();
        
        try {
            this.conectar();
            ps = this.getCnx().prepareStatement(sql);
            ps.setString(1, u.getUsuario());
            ps.setString(2, u.getPass());
            
            rs = ps.executeQuery();
            while(rs.next()){
                
                u.setUsuario(rs.getString(1));
                u.setPass(rs.getString(2));
                list.add(u);
            }
            
            return list;
        } catch (Exception e) {
            return null;
        }
    }
    
    public boolean insertarUsuario(Usuarios u){
        sql = "INSERT INTO usuarios VALUES(?,?,?,?,?)";
        
        try {
            this.conectar();
            ps = this.getCnx().prepareStatement(sql);
            ps.setInt(1,u.getId_usuario());
            ps.setString(2, u.getNombre());
            ps.setString(3, u.getApellido());
            ps.setString(4, u.getUsuario());
            ps.setString(5, u.getPass());
            
            return ps.execute();
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean insertarUsuarioCom(UsuarioCompetencia ucom){
        sql = "INSERT INTO usuario_competencia VALUES(?,?)";
        
        try {
            this.conectar();
            ps = this.getCnx().prepareStatement(sql);
            ps.setInt(1,ucom.getUsuarios().getId_usuario());
            ps.setInt(2, ucom.getCompetencias().getId_competencia());            
            
            return ps.execute();
        } catch (Exception e) {
            return false;
        }
    }
}
