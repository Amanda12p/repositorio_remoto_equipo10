package Conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class conexion {

    //Variables para la conexión
    private static String bd = "equipo10";
    private static String user = "kz";
    private static String pass = "kzroot";
    private static String url = "jdbc:mysql://usam-sql.sv.cds:3306/" + bd + "?useSSL=false";

    Connection cnx;

    public Connection getCnx() {
        return cnx;
    }

    public void setCnx(Connection cnx) {
        this.cnx = cnx;
    }

    public void conectar() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.cnx = DriverManager.getConnection(url, user, pass);
            if (this.cnx != null) {
                System.out.println("Conexión realizada.");
            } else {
                System.out.println("Fallo al conectar.");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void desconectar() throws Exception {
        if (this.cnx != null) {
            if (this.cnx.isClosed() != true) {
                try {
                    this.cnx.close();
                } catch (Exception e) {
                    throw e;
                }
            }
        }
    }

}
