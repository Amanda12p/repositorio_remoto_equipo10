/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author christian.ramirezusa
 */
public class UsuarioCompetencia {
    
    private Usuarios usuarios;
    private Competencias competencias;

    public Usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    public Competencias getCompetencias() {
        return competencias;
    }

    public void setCompetencias(Competencias competencias) {
        this.competencias = competencias;
    }
        
}
