package Controller;

import Dao.CompetenciasDao;
import Model.Competencias;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.metamodel.SetAttribute;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CompetenciasServlet", urlPatterns = {"/CompetenciasServlet"})
public class CompetenciasServlet extends HttpServlet {

    CompetenciasDao comDao = new CompetenciasDao();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "consultar":
                consultar(request, response);
                break;
            case "insertar":
                insertar(request, response);
                break;
            case "editar":
                editar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "nuevo":
                nuevo(request, response);
                break;
        }
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            request.setAttribute("listaCompetencias", comDao.consultar());
            RequestDispatcher rd;
            rd = request.getRequestDispatcher("View/competencias.jsp");
            rd.forward(request, response);

        } catch (Exception e) {
        }

    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombre = request.getParameter("txtNombre");
        String descripcion = request.getParameter("txtDescripcion");
        Competencias comp = new Competencias();
        comp.setNombre(nombre);
        comp.setDescripcion(descripcion);
        try {
            comDao.insertar(comp);
            request.setAttribute("listaCompetencias", comDao.consultar());
            RequestDispatcher rd;
            rd = request.getRequestDispatcher("View/competencias.jsp");
            rd.forward(request, response);
        } catch (Exception e) {
        }
    }

    protected void editar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    protected void nuevo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("View/nuevaCompetencia.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
