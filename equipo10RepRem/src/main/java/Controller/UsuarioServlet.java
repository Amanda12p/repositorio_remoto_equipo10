/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.CompetenciasDao;
import Dao.UsuarioDao;
import Model.Competencias;
import Model.UsuarioCompetencia;
import Model.Usuarios;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author christian.ramirezusa
 */
@WebServlet(name = "UsuarioServlet", urlPatterns = {"/UsuarioServlet"})
public class UsuarioServlet extends HttpServlet {

    CompetenciasDao comDao = new CompetenciasDao();
    UsuarioDao usDao = new UsuarioDao();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        String action = request.getParameter("action");
        switch (action) {
            case "consultarUser":
                consultarAllUsuario(request, response);
                break;
            case "consultarCompe":
                consultarUserCompe(request, response);
                break;
            case "insertar":
                insertar(request, response);
                break;

            case "insertarUserCom":
                insertarUserCom(request, response);
                break;
            case "editar":
                editar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "login":
                login(request, response);
                break;

            case "nuevo":
                nuevo(request, response);
                break;

            case "listas":
                listas(request, response);
                break;
        }
    }

    protected void consultarUserCompe(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            request.setAttribute("listaUserCom", usDao.listaUsuariosCom());
            RequestDispatcher rd;
            rd = request.getRequestDispatcher("View/usuarios.jsp");
            rd.forward(request, response);

        } catch (Exception e) {
        }

    }

    protected void consultarAllUsuario(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            request.setAttribute("listaUser", usDao.listaUsuariosAll());
            RequestDispatcher rd;
            rd = request.getRequestDispatcher("View/usuarios.jsp");
            rd.forward(request, response);

        } catch (Exception e) {
        }

    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombre = request.getParameter("txtNombre");
        String apellido = request.getParameter("txtApellido");
        String usuario = request.getParameter("txtUsuario");
        String pass = request.getParameter("txtPass");

        Usuarios user = new Usuarios(0);

        user.setNombre(nombre);
        user.setApellido(apellido);
        user.setUsuario(usuario);
        user.setPass(pass);

        try {
            usDao.insertarUsuario(user);
            consultarAllUsuario(request, response);
        } catch (Exception e) {
            String msg = "Usuario no se agrego";
            request.setAttribute("msg", msg);
            RequestDispatcher rd;
            rd = request.getRequestDispatcher("View/agregarUsuario.jsp");
            rd.forward(request, response);
        }

    }

    protected void insertarUserCom(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int usuario = Integer.parseInt(request.getParameter("txtUsuario"));
        int compe = Integer.parseInt(request.getParameter("txtCompe"));

        Usuarios user = new Usuarios();
        Competencias comp = new Competencias();
        UsuarioCompetencia usercomp = new UsuarioCompetencia();
        UsuarioDao userD = new UsuarioDao();

        user.setId_usuario(usuario);
        comp.setId_competencia(compe);

        usercomp.setUsuarios(user);
        usercomp.setCompetencias(comp);

        try {
            userD.insertarUsuarioCom(usercomp);
            usuarioCompetencia(request, response);
        } catch (Exception e) {
            String msg = "Usuario no se agrego";
            request.setAttribute("msg", msg);
            RequestDispatcher rd;
            rd = request.getRequestDispatcher("View/asignarCompetencia.jsp");
            rd.forward(request, response);
        }

    }

    protected void editar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    protected void listas(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        RequestDispatcher rd;

        UsuarioDao userD = new UsuarioDao();
        CompetenciasDao cmpD = new CompetenciasDao();

        request.setAttribute("listU", userD.listaUsuariosAll());
        request.setAttribute("listComp", cmpD.consultar());

        rd = request.getRequestDispatcher("View/asignarCompetencia.jsp");
        rd.forward(request, response);
    }
    
    protected void usuarioCompetencia(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        RequestDispatcher rd;

        UsuarioDao userD = new UsuarioDao();
        CompetenciasDao cmpD = new CompetenciasDao();

        request.setAttribute("listaCompetencia", userD.listaUsuariosCom());
        //request.setAttribute("listComp", cmpD.consultar());

        rd = request.getRequestDispatcher("View/usuariosCompetencia.jsp");
        rd.forward(request, response);
    }

    protected void login(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Usuarios usu = new Usuarios();
        UsuarioDao usuD = new UsuarioDao();

        String user, pass;

        user = request.getParameter("txtUser");
        pass = request.getParameter("txtPass");

        usu.setUsuario(user);
        usu.setPass(pass);

        //usuD.login(usu);
        String msg;
        RequestDispatcher rd;

        if (usuD.login(usu).isEmpty()) {
            msg = "Usuario no registrado";
            request.setAttribute("msg", msg);
            rd = request.getRequestDispatcher("index.jsp");
            rd.forward(request, response);
        } else {
            msg = "Bienvenido: " + usu.getUsuario();
            request.setAttribute("msg", msg);
            rd = request.getRequestDispatcher("menu.jsp");
            rd.forward(request, response);
        }
    }

    protected void nuevo(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("View/agregarUsuario.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(UsuarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(UsuarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
