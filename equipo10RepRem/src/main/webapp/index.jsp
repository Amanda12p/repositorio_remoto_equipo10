<%-- 
    Document   : index
    Created on : 01-23-2020, 02:56:38 PM
    Author     : amanda.pinedausam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link href="resources/css/materialize.css" rel="stylesheet" type="text/css"/>
          <link href="resources/css/estilo.css" rel="stylesheet" type="text/css"/>
        <style>
            
        </style>
    </head>

    <body ng-app="mainModule">
        <div class="row">
                        <div class="input-field col s12">   
                            <center><output><h2>Ingreso usuarios</h2></output></center>                        
                        </div>
                    </div>
        <div id="login-page" class="row">
            <div class="col s12 z-depth-6 card-panel">
                <form class="login-form" method="POST" action="UsuarioServlet?action=login">
                    <div class="row">
                    </div>
                    <div class="row">
                        <div class="input-field col s12">   
                            <output>Usuario</output>
                            <input class="validate" name="txtUser" id="txtUser" type="text">                   
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">  
                            <output>Contraseña</output>
                            <input class="validate" name="txtPass" id="txtPass" type="password">                         
                        </div>
                    </div>               
                    <div class="row">
                        <div class="input-field col s12">
                            <button   type='submit' class="btn waves-effect waves-light col s12" >Ingresar</button>
                        </div>
                    </div>    

                </form>
                <label>${msg}</label>
            </div>
        </div> </body>
     
</html>
