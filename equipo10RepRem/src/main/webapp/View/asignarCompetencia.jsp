<%-- 
    Document   : asignarCompetencia
    Created on : 01-24-2020, 01:52:59 PM
    Author     : christian.ramirezusa
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="resources/css/materialize.css" rel="stylesheet" type="text/css"/>
        <script src="resources/js/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="resources/js/materialize.js" type="text/javascript"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <h1>Hello World!</h1>
            <div class="row">
                <form method="POST" action="UsuarioServlet?action=insertarUserCom">


                    <div class="col m3">
                        <select name="txtUsuario">
                            <c:forEach items="${listU}" var="u">
                                <option value="${u.id_usuario}">${u.nombre} ${u.apellido}</option>            
                            </c:forEach>
                        </select>
                    </div>
                    <div class="col m3">
                        <select name="txtCompe">
                            <c:forEach items="${listComp}" var="comp">
                                <option value="${comp.id_competencia}">${comp.nombre}</option>            
                            </c:forEach>
                        </select>
                    </div>
                    <input type="submit" value="Asignar competencia"/>
                </form>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $('select').formSelect();
            });
        </script>

    </body>
</html>
