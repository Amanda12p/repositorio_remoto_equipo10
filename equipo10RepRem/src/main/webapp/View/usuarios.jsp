<%-- 
    Document   : usuarios
    Created on : 01-24-2020, 10:39:13 AM
    Author     : christian.ramirezusa
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Usuarios</title>
        <link href="resources/css/materialize.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="row">
            <div class="col m3">

            </div>
            <div class="col m6">
                <h1>Usuarios</h1>
                <table border="1" width="100%" class="table responsive-table">
                    <thead>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Usuario</th>
                    </thead>

                    <tbody>
                        <c:forEach items="${listaUser}" var="u">
                            <tr>
                                <td>${u.id_usuario}</td>
                                <td>${u.nombre}</td>
                                <td>${u.apellido}</td>
                                <td>${u.usuario}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <br>
                <a href="UsuarioServlet?action=nuevo" class="btn"/>Nuevo</a>
                
            </div></div>
    </body>
</html>

