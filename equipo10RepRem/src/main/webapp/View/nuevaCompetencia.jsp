<%-- 
    Document   : nuevaCompetencia
    Created on : 01-24-2020, 09:20:36 AM
    Author     : amanda.pinedausam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nueva competencia</title>
        <link href="resources/css/materialize.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <center><h1>Nueva Competencia</h1></center>
            <br>
            
            <form method="post" action="CompetenciasServlet?action=insertar">
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m8">
                        <label>Nombre:</label>
                        <input type="text" name="txtNombre"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m8">
                        <label>Descripción:</label>
                        <input type="text" name="txtDescripcion"/> 
                    </div>
                </div>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m8">
                        <input type="submit" value="Enviar" class="btn"/>
                    </div>
                </div>

            </form>
        </div>

    </body>
</html>
