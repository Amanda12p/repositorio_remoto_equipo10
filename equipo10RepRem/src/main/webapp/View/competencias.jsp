<%-- 
    Document   : competencias
    Created on : 01-24-2020, 08:36:04 AM
    Author     : amanda.pinedausam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Competencias</title>
        <link href="resources/css/materialize.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="row">
            <div class="col m3">

            </div>
            <div class="col m6">
                <h1>Competencias</h1>
                <table border="1" width="100%" class="table responsive-table">
                    <tr>
                        <td>Id</td>
                        <td>Nombre</td>
                        <td>Descripción</td>       
                    </tr>
                    <c:forEach var="com" items="${listaCompetencias}">
                        <tr>
                            <td>${com.id_competencia}</td>
                            <td>${com.nombre}</td>
                            <td>${com.descripcion}</td>                  				
                        </tr>
                    </c:forEach>
                </table>
                <br>                
                <a href="CompetenciasServlet?action=nuevo" class="btn"/>Nuevo</a>
            </div>          
        </div>
    
</body>
</html>
