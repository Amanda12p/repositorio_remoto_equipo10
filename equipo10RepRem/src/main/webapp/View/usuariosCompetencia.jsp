<%-- 
    Document   : usuarios
    Created on : 01-24-2020, 10:39:13 AM
    Author     : christian.ramirezusa
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Usuarios</title>
        <link href="resources/css/materialize.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="row">
            <div class="col m3">

            </div>
            <div class="col m6">
                <h1>Usuarios y sus Competencias</h1>
                <table border="1" width="100%" class="table responsive-table">
                    <thead>                    
                    <th>Nombre</th>
                    <th>Apellido</th>                    
                    <th>Competencia</th>
                    </thead>

                    <tbody>
                        <c:forEach items="${listaCompetencia}" var="u">
                            <tr>
                                <td>${u.usuarios.nombre}</td>
                                <td>${u.usuarios.apellido}</td>
                                <td>${u.competencias.nombre}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <br>
                <a href="UsuarioServlet?action=listas" class="btn"/>Nuevo</a>
                
            </div></div>
    </body>
</html>

