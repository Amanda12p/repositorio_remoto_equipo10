<%-- 
    Document   : agregarUsuario
    Created on : 01-24-2020, 11:32:13 AM
    Author     : christian.ramirezusa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nuevo usuario</title>
        <link href="resources/css/materialize.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
         <div class="container">
             <center><h1>Usuarios</h1></center>
            <br>
            
            <form method="post" action="UsuarioServlet?action=insertar">
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m8">
                        <label>Nombre:</label>
                        <input type="text" name="txtNombre"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m8">
                        <label>Apellido:</label>
                        <input type="text" name="txtApellido"/> 
                    </div>
                </div>
                 <div class="row">
                    <div class="col m3"></div>
                    <div class="col m8">
                        <label>Usuario:</label>
                        <input type="text" name="txtUsuario"/> 
                    </div>
                </div>
                 <div class="row">
                    <div class="col m3"></div>
                    <div class="col m8">
                        <label>Contraseña:</label>
                        <input type="text" name="txtPass"/> 
                    </div>
                </div>          
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m8">
                        <input type="submit" value="Enviar" class="btn"/>
                    </div>
                </div>
            </form>            
        </div>
    </body>
</html>
