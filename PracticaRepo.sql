CREATE DATABASE if not exists equipo10;
USE equipo10;
-- drop database equipo10

CREATE TABLE competencias
(
	id_competencia INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(20),
    descripcion VARCHAR(20)
);

CREATE TABLE usuarios
(
	id_usuario  INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    apellido VARCHAR(50) NOT NULL,
    usuario VARCHAR(10) NOT NULL,
    pass VARCHAR(20) NOT NULL
);


CREATE TABLE usuario_competencia
(
	id_usuario INT NOT NULL,
    id_competencia INT NOT NULL
);

ALTER TABLE usuario_competencia ADD CONSTRAINT fk_usuario FOREIGN KEY(id_usuario) REFERENCES usuarios(id_usuario) ;
ALTER TABLE usuario_competencia ADD CONSTRAINT fk_competencia FOREIGN KEY(id_competencia) REFERENCES competencias(id_competencia) ;

-- INSERT INTO competencias VALUES(0,'Linux','Sistema Operativo');

/*SELECT b.usuario, c.nombre FROM usuario_competencia a
INNER JOIN usuarios b ON b.id_usuario = a.id_usuario
INNER JOIN competencias c ON c.id_competencia = a.id_competencia;*/

INSERT INTO usuarios(nombre, apellido, usuario, pass) VALUES('Amanda','Pineda','user1','123'),('Christian','Ramirez','user2','123');
INSERT INTO competencias(nombre, descripcion) VALUES('c#','Programación'),('JAVA','Programación');
SELECT * FROM Usuarios WHERE usuario = 'user' AND pass = '123';
